package client;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.Naming;

import service.UserService;

public class UserClient {
	
	private static UserService server;

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, Exception {
		server = (UserService) Naming.lookup("//localhost/MyServer");
		System.out.println(server.register("krrish", "krrish"));
		server.work();
		System.out.println(server.login("krrish", "krrish"));
		System.out.println(server.register("krrishg", "krrish"));
		server.rest();
		System.out.println(server.login("krrish", "krrish"));
		System.out.println(server.register("krrish", "krrishg"));
	}
}
