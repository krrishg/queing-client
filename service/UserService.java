package service;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface UserService extends Remote {
	String login(String username, String password) throws RemoteException;
	String register(String username, String password) throws RemoteException;
	void work() throws Exception;
	void rest() throws Exception;
}
